Feature: Nobit homepage
  As a nobits user
  I should able to see list of nobits
  
  Scenario: Listing all nobits at homepage
    Given I am on "/nobits/?doconmeo=v10810"
    Then I should see "Type of notification"

  Scenario: Displaying 404 page when visiting non-existent  page
    Given I am on "/nobits/404page/?doconmeo=v10810"
    Then I should see "404 Not Found"